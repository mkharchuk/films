<?php include_once ROOT . '/views/layouts/header.php';?>

            <article class="box post post-excerpt">
                <table class="simple-little-table" cellspacing='0'>
                    <tr>
                        <th colspan="2" style="text-align: center; font-size: larger">Movie details</th>
                    </tr>
                    <tr>
                        <td>Movie title:</td>
                        <td><? echo $filmItem['title']; ?></td>
                    </tr>
                    <tr>
                        <td>Release Year:</td>
                        <td><? echo $filmItem['release_year']; ?></td>
                    </tr>
                    <tr>
                        <td>Format:</td>
                        <td><? echo $filmItem['format']; ?></td>
                    </tr>
                    <tr>
                        <td>Actors:</td>
                        <td><? echo $filmItem['stars']; ?></td>
                    </tr>
                </table>
                <a href="/">Back to the movie list</a>
            </article>
<?php include_once ROOT . '/views/layouts/footer.php';?>