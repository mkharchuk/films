<?php include_once ROOT . '/views/layouts/header.php'; ?>
    <article class="box post post-excerpt">
        <form action="" method="post">
            <label>Title</label>
            <input type="text" name="title">
            <label>Release Year</label>
            <input type="text" name="year">
            <label>Format</label>
            <select name="format">
                <?php foreach ($format as $item): ?>
                    <option value="<?php echo $item['id_format']; ?>"><?php echo $item['name']; ?></option>
                <?php endforeach; ?>
            </select>
            <label>Actors</label>
            <input type="text" name="actors" placeholder="Example: Name Surname 1, Name Surname 2, etc.">
            <br>
            <input type="submit" name="submit" value="Add">
        </form>
    </article>
<?php include_once ROOT . '/views/layouts/footer.php'; ?>