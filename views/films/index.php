<?php include_once ROOT . '/views/layouts/header.php';?>
    <article class="box post post-excerpt" xmlns="http://www.w3.org/1999/html">

                <form name="search" method="post" action="">
                    <input type="search" name="query" placeholder="Search" id="search">
                    <input type="submit" value="Go" id="searchButton" name="searchSubmit">
                   <div style="display: inline-block; margin-left: 4%"> <label><input name="r1" type="radio" value="rtitle" checked>Search by title
                        <label><input name="r1" type="radio" value="ractor">Search by  actor name</label>
                    </label></div>
                </form>
                    <?php
                    if(!empty($filmList)):
                    ?>
                    <table class="simple-little-table" cellspacing='0' id="grid">
                    <thead>
                        <tr>
                            <th>Movie title</th>
                            <th>Release Year</th>
                            <th>Format</th>
                        </tr><!-- Table Header -->
                    <thead>
                    <tbody>
                    <?php

                        foreach ($filmList as $filmItem):
                        ?>
                        <tr>
                            <td><a href="/<? echo $filmItem['id_film']; ?>"><? echo $filmItem['title']; ?></a></td>
                            <td><? echo $filmItem['release_year']; ?></td>
                            <td><? echo $filmItem['format']; ?></td>
                            <td><a href="delete/<? echo $filmItem['id_film']; ?>"><img src="/template/images/delete.png"></a></td>
                        </tr><!-- Table Row -->
                    <?php
                        endforeach;
                    else:
                        echo "<div class='searcResult'>Sorry, there are no movies in the base</div>";
                    endif;
                    ?>
                    </tbody>
                </table>
            </article>
<?php include_once ROOT . '/views/layouts/footer.php';?>