
</div>
</div>

<!-- Sidebar -->
<div id="sidebar">

    <!-- Logo -->
    <h1 id="logo"><a href="/">AppFilms</a></h1>

    <!-- Nav -->
    <nav id="nav">
        <ul>
            <li><a href="/">Movie list</a></li>
            <li><a href="/add">Add movies</a></li>
            <li><a href="/sort">Sort movies by title</a></li>
            <li><a href="/import">Import movies</a></li>
        </ul>
    </nav>

</div>

</div>
<script src="/template/js/jquery.min.js"></script>
<script src="/template/js/custom.js"></script>
<script src="/template/js/skel.min.js"></script>
<script src="/template/js/skel-layers.min.js"></script>
<script src="/template/js/init.js"></script>
</body>
</html>