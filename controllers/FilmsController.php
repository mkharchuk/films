<?php
include_once ROOT . '/models/Films.php';
class FilmsController
{
    public function actionIndex()
    {
        $filmList = Films::getFilmList();
        if (isset($_POST['searchSubmit'])) {
            $query = $_POST['query'];

            if ($_POST['r1'] == 'rtitle') {
                //title
                $filmList = Films::searcFilm(trim($query));
                require_once ROOT . '/views/films/index.php';
            }
            else {
                $filmList = Films::searcFilmByActor($query);
                require_once ROOT . '/views/films/index.php';
            }
        }
        require_once ROOT . '/views/films/index.php';
        return true;
    }

    public function actionView($id)
    {
        $filmItem = Films::getFilmItemById($id);
        require_once ROOT . '/views/films/viewDetails.php';
        return true;
    }

    public function actionDelete($id)
    {
        if (Films::deleteFilmItemById($id)) {
            header('Location: /films');
        }
        return true;
    }

    public function actionAdd()
    {
        $title = '';
        $year = '';
        $format = 0;
        $actors = '';
        if (isset($_POST['submit'])) {
            $title = $_POST['title'];
            $year = $_POST['year'];
            $format = $_POST['format'];
            $actors = $_POST['actors'];
            $fid = Films::insertFilm($title, $year, $format);

            $actor = explode(', ', $actors);
            foreach ($actor as $item){
                $sid = Films::checkActor($item);
                if($sid){
                    Films::insertFilmActor($fid, $sid);
                }
                else {
                    $sid = Films::insertActor($item);
                    Films::insertFilmActor($fid, $sid);
                }


            }
        }
        $format = Films::getFormat();
        require_once ROOT . '/views/films/addFilm.php';
        return true;
    }
    public function actionSort()
    {
        function cmp($a, $b){
            return strcmp($a["title"], $b["title"]);
        }
        $filmList = Films::getFilmList();
            if (isset($_POST['searchSubmit'])) {
                $query = $_POST['query'];

                if ($_POST['r1'] == 'rtitle') {
                    //title
                    $filmList = Films::searcFilm(trim($query));
                    usort($filmList, 'cmp');
                    require_once ROOT . '/views/films/index.php';
                }
                else {
                    //actor

                }
            }
        usort($filmList, 'cmp');
        require_once ROOT . '/views/films/index.php';
        return true;
    }

    public function actionImport()
    {
        if (isset($_POST['import'])){
            $row = 1;
            $i = 0;
            $handle = fopen($_FILES['txt']['tmp_name'], "r");
            while (($data = fgetcsv($handle, 100, ":")) !== FALSE) {
                $num = count($data);
                if ($num == 1) {
                    $row--;
                }
                else {
                    $key = str_replace(' ', '_', $data[0]);

                    $mas[$i][strtolower($key)] = $data[1];
                }

                if ($row == 4){
                    $i++;
                    $row = 0;
                }

                $row++;
            }

            foreach ($mas as $item) {
                $item['title'] = trim($item['title']);
                $item['format'] = trim($item['format']);
                $item['release_year'] = trim($item['release_year']);
                $item['stars'] = trim($item['stars']);
                $formatId = Films::getFormatId(trim($item['format']));
                if(Films::checkFilm($item['title']) == false) {
                    $fid = Films::insertFilm($item['title'], $item['release_year'], $formatId);
                    $actor = explode(', ', $item['stars']);
                    foreach ($actor as $value) {
                        $sid = Films::checkActor($value);
                       if ($sid == false) {
                           $sid = Films::insertActor($value);
                           Films::insertFilmActor($fid, $sid);
                       }
                       else {
                           Films::insertFilmActor($fid, $sid);
                       }
                    }

                }
            }

        }
        require_once ROOT . '/views/films/importFilms.php';
        return true;
    }
}

?>