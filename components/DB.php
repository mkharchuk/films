<?php

class DB
{
    public static function connection()
    {
        $params = include ROOT . '/config/db_config.php';

        $dsn = "mysql:host={$params['host']};dbname={$params['db_name']}";
        return new PDO($dsn, $params['username'], $params['password']);
    }
}