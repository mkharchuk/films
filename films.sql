SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- База даних: `films`
--

-- --------------------------------------------------------

--
-- Структура таблиці `f2s`
--

CREATE TABLE `f2s` (
  `id` int(11) NOT NULL,
  `id_film` int(11) NOT NULL,
  `sid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `film_data`
--

CREATE TABLE `film_data` (
  `id_film` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `release_year` varchar(5) NOT NULL,
  `format` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблиці `format`
--

CREATE TABLE `format` (
  `id_format` int(11) NOT NULL,
  `name` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `format`
--

INSERT INTO `format` (`id_format`, `name`) VALUES
(1, 'VHS'),
(2, 'Blu-Ray'),
(3, 'DVD');

-- --------------------------------------------------------

--
-- Структура таблиці `stars`
--

CREATE TABLE `stars` (
  `sid` int(11) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `f2s`
--
ALTER TABLE `f2s`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `film_data`
--
ALTER TABLE `film_data`
  ADD PRIMARY KEY (`id_film`);

--
-- Індекси таблиці `format`
--
ALTER TABLE `format`
  ADD PRIMARY KEY (`id_format`);

--
-- Індекси таблиці `stars`
--
ALTER TABLE `stars`
  ADD PRIMARY KEY (`sid`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `f2s`
--
ALTER TABLE `f2s`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `film_data`
--
ALTER TABLE `film_data`
  MODIFY `id_film` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `format`
--
ALTER TABLE `format`
  MODIFY `id_format` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблиці `stars`
--
ALTER TABLE `stars`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT;COMMIT;
