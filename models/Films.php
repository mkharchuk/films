<?php

class Films
{
    public static function getFilmItemById($id)
    {
        $id = intval($id);
        if ($id) {

            $db = DB::connection();

            $query = $db->query('SELECT fd.title, fd.release_year, f.name as format '
                . 'FROM film_data fd INNER JOIN format f ON fd.format=f.id_format'
                . ' WHERE id_film =' . $id);
            $filmItem = $query->fetch(2);
            $query = $db->query('SELECT s.name '
                . 'FROM stars s INNER JOIN f2s ON s.sid=f2s.sid'
                . ' WHERE f2s.id_film =' . $id);
            $tmp = '';
            while($row = $query->fetch(2))
            {
                $tmp .= $row['name'] . ', ';
            }
            $filmItem['stars'] = trim($tmp, ', ');
            return $filmItem;
        }
    }

    public static function getFilmList()
    {
        $db = DB::connection();

        $query = $db->query('SELECT fd.id_film, fd.title, fd.release_year, f.name as format '
            . 'FROM film_data fd INNER JOIN format f ON fd.format=f.id_format');

        return $query->fetchAll(2);;
    }

    public static function deleteFilmItemById($id)
    {
        $id = intval($id);
        if ($id) {
            $db = DB::connection();
            $query = $db->query('DELETE FROM f2s WHERE id_film=' . $id);
            $result = $query->execute();
            if ($result) {
                $query = $db->query('DELETE FROM film_data WHERE id_film=' . $id);
                $result = $query->execute();
                return $result;
            }

        }

    }
    public static function getFormat()
    {
        $db = DB::connection();
        $query = $db->query('SELECT * FROM  format');
        return $query->fetchAll(2);
    }

    public static function insertActor($name)
    {
        $db = DB::connection();
        $query = $db->prepare('INSERT INTO stars (name)'.
            'VALUES (:name)');
        $query->bindParam(':name', $name, PDO::PARAM_STR);
        $query->execute();
        return $db->lastInsertId();
    }

    public static function checkActor($name)
    {
        $db = DB::connection();
        $query = $db->prepare('SELECT sid FROM stars WHERE name = :name');
        $query->bindParam(':name', $name, PDO::PARAM_STR);
        $query->execute();
        if ($query->rowCount() != 0) {
            return  $query->fetchColumn();
        }
        return false;
    }
    public static function  insertFilm($title, $year, $format)
    {
        $db = DB::connection();
        $query = $db->prepare('INSERT INTO film_data (title, release_year, format)'.
            'VALUES (:title, :release_year, :format)');
        $query->bindParam(':title', $title, PDO::PARAM_STR);
        $query->bindParam(':release_year', $year, PDO::PARAM_STR);
        $query->bindParam(':format', $format, PDO::PARAM_INT);
        $query->execute();
        return $db->lastInsertId();
    }

    public static function  insertFilmActor($fid, $sid)
    {
        $db = DB::connection();
        $query = $db->prepare('INSERT INTO f2s (id_film, sid)'.
            'VALUES (:id_film, :sid)');
        $query->bindParam(':id_film', $fid, PDO::PARAM_INT);
        $query->bindParam(':sid', $sid, PDO::PARAM_INT);
        $query->execute();
    }
    public static function  searcFilm($queryStr)
    {
        $queryStr = '%' . $queryStr . '%';
        $db = DB::connection();
        $query = $db->prepare('SELECT fd.id_film, fd.title, fd.release_year, f.name as format FROM film_data fd INNER JOIN format f ON fd.format=f.id_format WHERE title LIKE :queryStr');
        $query->bindParam(':queryStr', $queryStr, PDO::PARAM_STR);
        $query->execute();
        return $query->fetchAll(2);
    }

    public static function  searcFilmByActor($queryStr)
    {
        $db = DB::connection();
        $sid = self::checkActor($queryStr);
        if ($sid) {
            $query = $db->prepare('SELECT id_film FROM f2s WHERE sid = :sid');
            $query->bindParam(':sid', $sid, PDO::PARAM_INT);
            $query->execute();
            $fid = $query->fetchAll(2);
            $i = 0;
            foreach ($fid as $item) {
                $query = $db->prepare('SELECT fd.id_film, fd.title, fd.release_year, f.name as format FROM film_data fd INNER JOIN format f ON fd.format=f.id_format WHERE id_film = :fid');
                $query->bindParam(':fid', $item['id_film'], PDO::PARAM_INT);
                $query->execute();
                $filmList[$i] = $query->fetch(2);
                $i++;
            }
            return $filmList;
        }


        return '';
    }
    public static function getFormatId($format)
    {
        $db = DB::connection();
        $query = $db->prepare('SELECT id_format FROM format WHERE name = :name');
        $query->bindParam(':name', $format, PDO::PARAM_STR);
        $query->execute();
        return $query->fetchColumn();
    }

    public static function checkFilm($title)
    {
        $db = DB::connection();
        $query = $db->prepare('SELECT id_film FROM film_data WHERE title = :title');
        $query->bindParam(':title', $title, PDO::PARAM_STR);
        $query->execute();
        if ($query->rowCount() != 0) {
            return true;
        }
        else{
            return false;
        }
    }


}