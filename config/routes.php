<?php
return array(
    '([0-9]+)' => 'films/view/$1',
    'delete/([0-9]+)' => 'films/delete/$1',
    'add' => 'films/add',
    'sort' => 'films/sort',
    'import' => 'films/import',
	'/' => 'films/index',
);